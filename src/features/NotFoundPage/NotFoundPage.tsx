const NotFoundPage = () => {
  return (
    <div>
      Page not found.
      <a href="/">Click here to go back home.</a>
    </div>
  );
};

export default NotFoundPage;

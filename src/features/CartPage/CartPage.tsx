import { useMemo, useState } from "react";
import {
  changeQuantity,
  decreaseQuantity,
  increaseQuantity,
  removeFromCart,
} from "../../slices/cartSlice";
import { useAppDispatch, useAppSelector } from "../../store";
import { NavLink } from "react-router-dom";
import { CartProduct } from "../models";

const numberToPriceString = (v: number) => {
  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(v);
};

const CartPage = () => {
  const cartItems = useAppSelector((state) => state.cartSlice.value);
  const dispatch = useAppDispatch();

  const total = useMemo(
    () =>
      numberToPriceString(
        cartItems.reduce((acc, v) => (acc += v.quantity * v.price), 0)
      ),
    [cartItems]
  );

  const [cardNumber, setCardNumber] = useState<string>("");

  const onCardNumberChange = (v: string) => {
    if (v.split(" ").join("").length > 16) {
      return;
    }
    setCardNumber(
      v
        .replace(/\D/g, "")
        .match(/.{1,4}/g)
        ?.join(" ") || ""
    );
  };

  const isFormInvalid = (): boolean => {
    return cartItems.length <= 0 || cardNumber.split(" ").join("").length != 16;
  };

  const onChangeQuantity = (item: CartProduct, quantity: number) => {
    if (!quantity) {
      const confirm = prompt(
        "Enter yes if you want to remove this product:",
        "yes"
      );
      if (confirm === "yes") {
        dispatch(changeQuantity({ item, quantity }));
      }
      return;
    }

    dispatch(changeQuantity({ item, quantity }));
  };

  return (
    <>
      {cartItems.length > 0 ? (
        <>
          <table style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>Title</th>
                <th>Quantity</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {cartItems.map((x) => (
                <tr key={x.id}>
                  <td>{x.title}</td>
                  <td style={{ display: "flex", gap: "8px" }}>
                    <button onClick={() => dispatch(decreaseQuantity(x))}>
                      -
                    </button>
                    <input
                      type="number"
                      value={x.quantity}
                      onChange={(e) => onChangeQuantity(x, +e.target.value)}
                    />
                    <button onClick={() => dispatch(increaseQuantity(x))}>
                      +
                    </button>
                    <button onClick={() => dispatch(removeFromCart(x))}>
                      x
                    </button>
                  </td>
                  <td>{numberToPriceString(x.quantity * x.price)}</td>
                </tr>
              ))}
            </tbody>
          </table>
          <h2>Total: {total}</h2>
        </>
      ) : (
        <>No items in cart.</>
      )}
      <div>
        <div style={{ display: "flex", gap: "8px" }}>
          <label>CreditCard:</label>
          <input
            type="text"
            value={cardNumber}
            onChange={(e) => onCardNumberChange(e.target.value)}
          />
          <button
            disabled={isFormInvalid()}
            onClick={() => alert("Checked out successfully!")}
          >
            Checkout
          </button>
        </div>
      </div>
      <div>
        <NavLink to="/">Or continue shopping</NavLink>
      </div>
    </>
  );
};

export default CartPage;

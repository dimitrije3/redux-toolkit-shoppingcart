import { NavLink } from "react-router-dom";
import { useAppSelector } from "../../store";
import { useMemo } from "react";

const NavBar = () => {
  const cartItems = useAppSelector((state) => state.cartSlice.value);
  const totalQuantity = useMemo(
    () => cartItems.reduce((acc, v) => (acc += v.quantity), 0),
    [cartItems]
  );
  return (
    <div style={{ width: "100%", display: "flex" }}>
      <NavLink to="/">Home</NavLink>
      {totalQuantity > 0 ? (
        <>
          <NavLink style={{ marginLeft: "auto" }} to="/cart">
            Cart Items: {totalQuantity}
          </NavLink>
        </>
      ) : (
        <></>
      )}
    </div>
  );
};

export default NavBar;

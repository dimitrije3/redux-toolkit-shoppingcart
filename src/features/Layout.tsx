import { Outlet } from "react-router-dom";
import NavBar from "./NavBar/NavBar";

const Layout = () => {
  return (
    <div style={{ background: "#fff", width: "100%", height: "100%" }}>
      <div
        style={{ display: "flex", paddingBlock: "20px", background: "#d3d3d3" }}
      >
        <div style={{ width: "768px", marginInline: "auto" }}>
          <NavBar></NavBar>
        </div>
      </div>
      <div style={{ width: "768px", marginInline: "auto" }}>
        <Outlet></Outlet>
      </div>
    </div>
  );
};

export default Layout;

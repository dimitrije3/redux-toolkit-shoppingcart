import { useEffect } from "react";
import useProducts from "../../hooks/useProducts";
import { useAppSelector, useAppDispatch } from "../../store";
import { setProducts } from "../../slices/productsSlice";
import { increaseQuantity } from "../../slices/cartSlice";
import NavBar from "../NavBar/NavBar";

const HomePage = () => {
  const { getProducts } = useProducts();

  const products = useAppSelector((state) => state.productsSlice.value);
  const dispatch = useAppDispatch();

  useEffect(() => {
    getProducts()
      .then((d) => {
        dispatch(setProducts(d));
      })
      .catch((e) => console.log(e));
  }, []);

  return (
    <table style={{ width: "100%" }}>
      <thead>
        <tr>
          <th>Title</th>
          <th>Category</th>
          <th>Price</th>
          <th>Action</th>
        </tr>
      </thead>

      <tbody>
        {products.map((x) => (
          <tr key={x.id}>
            <td>{x.title}</td>
            <td>{x.category}</td>
            <td>{x.price}</td>
            <td>
              <button onClick={() => dispatch(increaseQuantity(x))}>Add</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default HomePage;

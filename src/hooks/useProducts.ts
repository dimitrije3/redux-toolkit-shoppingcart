import { IProduct } from "../features/models";
import axios from "axios";
const BASE_URL = "https://dummyjson.com";

const useProducts = () => {
  const getProducts = async (
    skip: number = 0,
    limit: number = 10
  ): Promise<IProduct[]> => {
    try {
      const req = await axios.get(
        `${BASE_URL}/products?limit=${limit}&skip=${skip * limit}`
      );
      return req.data.products;
    } catch (error) {
      return [];
    }
  };

  return { getProducts };
};

export default useProducts;

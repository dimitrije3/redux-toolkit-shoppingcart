import { Outlet } from "react-router-dom";

function App() {
  return (
    <div className="bg-black" style={{ width: "500px" }}>
      <Outlet></Outlet>;
    </div>
  );
}

export default App;

import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { Provider } from "react-redux";
import { store } from "./store.ts";
import {
  Navigate,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import CartPage from "./features/CartPage/CartPage.tsx";
import HomePage from "./features/HomePage/HomePage.tsx";
import NotFoundPage from "./features/NotFoundPage/NotFoundPage.tsx";
import Layout from "./features/Layout.tsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout></Layout>,
    children: [
      {
        index: true,
        element: <HomePage></HomePage>,
      },
      {
        path: "/cart",
        element: <CartPage></CartPage>,
      },
      {
        path: "/404",
        element: <NotFoundPage></NotFoundPage>,
      },
      {
        path: "*",
        element: <Navigate to="/404"></Navigate>,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}></RouterProvider>
    </Provider>
  </React.StrictMode>
);

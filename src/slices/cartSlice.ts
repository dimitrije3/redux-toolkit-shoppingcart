import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { CartProduct, IProduct } from "../features/models";

export interface CartState {
  value: CartProduct[];
}

const localCart = localStorage.getItem("cart");

const initialState: CartState = {
  value: localCart && localCart != "undefined" ? JSON.parse(localCart) : [],
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    increaseQuantity: (state, action: PayloadAction<IProduct>) => {
      const existing = state.value.find((x) => x.id == action.payload.id);
      if (existing) {
        existing.quantity++;
      } else {
        state.value.push({ ...action.payload, quantity: 1 });
      }
    },
    decreaseQuantity: (state, action: PayloadAction<IProduct>) => {
      const existing = state.value.find((x) => x.id == action.payload.id);
      if (!existing) {
        return console.log("Item not found - will never happen");
      }
      existing.quantity--;
      if (existing.quantity === 0) {
        state.value = state.value.filter((x) => x.id !== existing.id);
      }
    },
    removeFromCart: (state, action: PayloadAction<CartProduct>) => {
      const existing = state.value.find((x) => x.id == action.payload.id);
      if (!existing) {
        return console.log("Nothing to remove - will never happen");
      }
      state.value = state.value.filter((x) => x.id !== existing.id);
    },
    changeQuantity: (
      state,
      action: PayloadAction<{ item: CartProduct; quantity: number }>
    ) => {
      const existing = state.value.find((x) => x.id == action.payload.item.id);
      if (!existing) {
        return console.log("Nothing to remove - will never happen");
      }
      existing.quantity = action.payload.quantity;
      if (existing.quantity === 0) {
        state.value = state.value.filter((x) => x.id !== existing.id);
      }
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  increaseQuantity,
  decreaseQuantity,
  removeFromCart,
  changeQuantity,
} = cartSlice.actions;

export default cartSlice.reducer;
